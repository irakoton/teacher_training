\section{Avec du public : déroulement détaillé et retours d'expérience} \label{sec:outline}

  Cette section décrit comment organiser une séance d'initiation à la cryptographie basée sur l'histoire présentée en Section~\ref{sec:story}, et en utilisant le matériel en Section~\ref{sec:material}.
  Sont inclus des retours d'expérience, ainsi que des anecdotes historiques et techniques pouvant ponctuer l'animation.
  
  \subsection{Introduction et Problème~\ref{q:v1}}
    \timeinfo{15 min}
    
    Après quelques minutes d'installation, l'activité commence avec une brève mise en contexte pouvant reprendre la Section~\ref{sec:intro}, en expliquant que le but de la séance est de se plonger dans le sujet en mettant la main à la pâte, via une énigme où la cryptographie est représentée par des coffres et cadenas.
    % 
    On passe ensuite à la présentation de l'énigme elle-même, en utilisant le matériel en illustration, jusqu'à poser le Problème~\ref{q:v1} au public (voir Section~\ref{sec:v1}).

    \begin{commentbox}[Retours d'expérience]
      \paragraph{Interaction avec le public}
        Il est important que cette première phase de l'activité soit interactive et mette le public à l'aise avec le format ludique.
        Les plus jeunes n'hésitent en général pas à proposer des idées, même simples et qui ne fonctionnent souvent pas --- mais cela permet de faire avancer la réflexion du groupe.
        Au contraire, les participants plus âgés identifient souvent mieux les défauts de leurs solutions et appréhendent donc souvent plus de prendre la parole.
        Il ne faut donc pas hésiter à \keyinfo{inciter à proposer des idées}, même s'ils savent qu'elles ne marchent pas, en leur expliquant que les formuler à haute voix aide à identifier les difficultés à résoudre.
        Une autre possibilité est de lancer le mouvement en proposant soi-même des solutions erronées, et de demander au public d'identifier pourquoi elles ne fonctionnent pas.
        En cas de question sur ce que peut faire chaque personnage :
        \begin{enumerate}
          \item Le livreur peut faire n'importe quoi \keyinfo{avec ce qu'on lui donne} (i.e., pas de cambriolage pour voler le gâteau ou les clés), et ne peut pas ouvrir un cadenas sans sa clé.
          \item Isabelle et Bob peuvent faire n'importe quoi tant que tout passe \keyinfo{par le livreur} (pas de communication par un canal annexe comme le téléphone par exemple).
          On considère aussi qu'il faut la clé pour \keyinfo{fermer} un cadenas.
        \end{enumerate}

      \paragraph{Propositions fréquentes}
        La plupart des protocoles sont \emph{attaquables} (i.e., le livreur peut voler le gâteau) ou \emph{invalides} (i.e., ne respectent pas l'énoncé).
        Quelques exemples :
        \begin{enumerate}[itemsep=1mm]
          \item \keyinfo{Attaquable}: 
          \emph{Isabelle envoie un coffre verrouillé avec le gâteau, puis la clé ultérieurement.}
          Le livreur peut alors obtenir le coffre verrouillé et la clé du cadenas.
          
          \item \keyinfo{Invalide}:
          \emph{Isabelle envoie un coffre verrouillé, attend un appel téléphonique de Bob comme accusé de réception pour s'assurer que le livreur n'a plus le colis, puis envoie la clé.} 
          Les canaux de communication annexes comme le téléphone sont interdits.
          La difficulté est précisément de lutter contre un attaquant contrôlant toutes les communications.
          
          \item \keyinfo{Attaquable}:
          \emph{Même chose, mais l'accusé de réception est un colis quelconque envoyé par Bob.}
          Le livreur peut très facilement fabriquer un faux accusé de réception.
          De toute façon, dans la vraie vie où les coffres verrouillés sont des messages chiffrés, le livreur pourrait facilement garder une \emph{copie} du colis :
          ces solutions à base d'accusés de réception n'ont donc pas d'intérêt en dehors de l'activité, et nous les mettons donc de côté.
          
          \item \keyinfo{Invalide}:
          \emph{Bob envoie son cadenas ouvert à Isabelle, qui l'utilise pour fermer le colis du gâteau, et l'envoie à Bob.}
          On rappelle qu'on suppose qu'Isabelle a besoin de la clé de Bob pour fermer le cadenas.
        \end{enumerate}
    \end{commentbox}

  \subsection{Attaque et Problème~\ref{q:v2}}
    \timeinfo{10 min}
    % \paragraph{L'attaque}
      Une fois la solution (\versionlab{1}) trouvée, l'animateur fait venir deux volontaires pour jouer le protocole physiquement, avec les coffres et les figurines :
      un jouera d'Isabelle, l'autre Bob, et l'animateur le livreur.
      Ne pas hésiter à le faire plusieurs fois.
      % 
      Une fois le public à l'aise avec le protocole, l'animateur le fait jouer une fois de plus mais réalise cette fois l'attaque sur \versionlab{1} (voir Section~\ref{sec:v1-atk}).
      C'est le moment de parler des livreurs passifs et actifs, d'introduire l'organisation gérant les cadenas publics, et de laisser le public résoudre rapidement, oralement, le Problème~\ref{q:v2} (voir Section~\ref{sec:v2}).

      \begin{commentbox}
        \customtriangleright\ \themaline{\emph{Détails et anecdotes}} :
        Le système initial de cadenas (où ils sont ouverts et fermés par la même clé) représente ce qu'on appelle le \keyinfo{chiffrement symétrique}.
        En revanche, le système de cadenas publics s'appelle le \keyinfo{chiffrement asymétrique} :
        tout le monde peut obtenir et fermer le cadenas de tout le monde, mais chacun ne peut ouvrir que le sien.
        % 
        Le chiffrement asymétrique, bien que plus puissant, nécessite cependant de mettre en place cette fameuse organisation fournissant les cadenas.
        Ici, nous lui faisons aveuglément confiance ;
        mais dans la vraie vie, c'est un problème complexe nécessitant de déployer des mécanismes de \keyinfo{certificats}.
        Si Isabelle demandait un cadenas mais qu'on lui en fournissait un autre, le gâteau serait à nouveau menacé !
        % 
        Vous avez d'ailleurs probablement déjà été confrontés au problème sur le web.
        Parfois, le navigateur rechigne à entrer sur un site car ``\emph{le certificat n'est pas valide}'' :
        cela signifie soit que
        \begin{enumerate*}
          \item quelqu'un essaie d'usurper ``l'identité'' du site  avec un faux certificat pour vous piéger, soit
          \item le site n'a pas ses certificats à jour.
        \end{enumerate*}
      \end{commentbox}

  \subsection{Phase de conception} \label{sec:conception}
    \timeinfo{25 min}

    On passe alors à la partie principale, où le public concevoir son propre protocole. 
    On commence par montrer l'attaque par empoisonnement sur \versionlab{2} avec les jouets.
    On pose ensuite le Problème~\ref{q:v3} (voir Section~\ref{sec:v3}), 
    puis sépare le public en \keyinfo{2 à 4 équipes de 3 à 5 personnes}, qui vont réfléchir au problème chacune de leur côté.
    Chacune reçoit aussi un minimum de \keyinfo{matériel} pour jouer physiquement les protocoles (boîte, figurines pour les personnages dont éventuellement l'organisation, 3 cadenas, post-it et stylo pour glisser des messages dans les colis).
    % 
    Après un temps de recherche en autonomie, pensez à \keyinfo{circuler dans les groupes} pour encourager ceux ayant du mal à démarrer, et signaler les protocoles invalides... mais \keyinfo{pas les attaques} ! 
    % Les trouver sera le travail des autres équipes dans la phase suivante.

    \begin{commentbox}[Retours d'expérience]
      % \paragraph{Logistique}
      %   % Une bonne \keyinfo{vingtaine de minutes} sont nécessaires pour avoir le temps de se confronter au problème.
      %   Il est important de laisser chercher un peu seul initialement, 
      %   puis de \keyinfo{circuler dans les groupes} après quelques minutes.
      %   Certains ont en effet du mal se lancer, mais se débloquent souvent quand on les incite à mettre les mains dans le cambouis :
      %   il est plus facile de réfléchir en partant d'un protocole naïf, d'identifier le mécanisme que le livreur exploite dans son attaque, puis de réfléchir à quoi ajouter pour l'en empêcher.
      %   % Même sans arriver à la solution, ils pourront présenter dans le pire des cas leur protocole partiel en l'état !
      %   % 
      %   Il faut cependant aussi laisser de l'autonomie aux groupes et ne \keyinfo{pas leur signaler} les failles de sécurité de leurs prototypes.
      %   Certains groupes, trop guidés, partent avec l'idée que ``ça n'a pas l'air si compliqué que ça''.
      %   Au contraire, les groupes confrontés à l'incertitude de si leur solution est correcte ou pas prennent souvent plus la mesure de la subtilité de l'exercice.
      % 
      % \paragraph{Erreurs classiques}
      Voici des exemples de protocoles \emph{invalides} vus sur le terrain :
      \begin{enumerate}[itemsep=1mm]
        \item \label{it:phone} \keyinfo{Utilisation de canaux de communication annexes}.
        Comme précédemment, les communications ne passant pas par le livreur (téléphone par exemple) sont interdites.
        % \item \label{it:diversion} \keyinfo{Diversion du livreur}.
        % Certains envoient des boîtes vident pour occuper le livreur quelque part pendant qu'ils vont autre chose. Ces groupes considèrent que le livreur ``ne peut pas être à plusieurs endroits à la fois'', et utilisent des stratagèmes comme envoyer des boîtes vides pour l'occuper quelque part pendant qu'Isabelle ou Bob fait autre chose.
        % Mais tous les colis doivent de toute façon passer par le livreur :
        % l'occuper ailleurs ne fait donc que retarder la prochaine étape du protocole, sans avantage en contrepartie.
        \item \label{it:obscurity} \keyinfo{Sécurité par obscurité} : ``\emph{Isabelle envoie cette boîte en prétendant qu'elle contient un message, alors qu'en fait c'est un gâteau, mais le livreur n'a aucun moyen de le savoir.}''

        On voit souvent ce genre de propositions reposant sur le fait que le livreur ne connaîtrait pas le protocole utilisé.
        Mais \keyinfo{le protocole ne peut pas être secret} :
        Isabelle l'utilise avec tous ses clients, dont le livreur (qui a aussi le droit d'acheter des gâteaux) !
        
        \item[\customtriangleright] \themaline{\emph{Détails et anecdotes}} :
        % l'idée de la sécurité par obscurité est naturelle :
        % si je cache mes clés sous le paillasson, tout ira bien puisque personne ne sait qu'elles sont là... 
        % du moins jusqu'à ce qu'on me voit les chercher à cet endroit.
        % En informatique c'est pareil :
        La sécurité par obscurité était déjà considérée comme une mauvaise pratique dans les principes de Kerckhoffs~\cite{kerk} en 1883, dans le contexte militaire.
        Aujourd'hui cela n'a pas changé : si la sécurité d'une entreprise repose sur le secret de son système, un employé part chez le concurrent et tout s'écroule !
        % si une banque ne peut protéger ses transactions que contre qui ne connaît pas leur système, alors n'importe quel employé est un attaquant potentiel ;
        % Il est en plus souvent possible de \emph{retro-ingénier} un système (deviner son fonctionnement en l'observant).

        % \item \keyinfo{Spécification du livreur} :
        % ``\emph{Isabelle envoie sa boîte, donc le livreur l'ouvre pour tenter de voler le gâteau, mais comme il n'y en a pas il en conclut que [...], et va donc [...]}''

        % Des groupes considèrent que le comportement du livreur suit une certaine logique (par ailleurs souvent dans un protocole par obscurité).
        % Les instructions d'un protocole ne concernent qu'Isabelle et Bob :
        % le livreur, lui, est \keyinfo{libre de ne pas les suivre} et fait ce qu'il veut, quand il veut. 
        % La question est alors de savoir s'il \emph{existe} une suite d'actions créant une brèche de sécurité, peu importe qu'elle paraisse absurde ou dénuée de logique.
        
        \item \label{it:sign} \keyinfo{Signature}.
        Il peut arriver que, pour authentifier un colis, des groupes fassent \emph{signer} un papier par Isabelle.
        Les \keyinfo{signatures digitales} existent en cryptographie mais ont des propriétés cruciales, en particulier que
        \begin{enumerate*}
          \item il est très difficile (impossible) d'imiter une signature valide de quelqu'un, et
          % \item on peut facilement vérifier à qui appartient une signature ;
          \item une signature valide sur un document subissant ultérieurement une modification perd sa validité.
          Dit autrement, trouver une signature valide sur un gâteau doit garantir que personne ne l'a empoisonné depuis sa signature.
        \end{enumerate*}
        Une signature papier n'a en général aucune de ces propriétés, et on les évite donc ici.

        \item \keyinfo{Déni de service} :
        ``\emph{Livreur peut juste voler le coffre verrouillé :
        certes, il ne pourra pas manger le gâteau, mais Bob ne recevra jamais son colis. 
        C'est un problème, non ?}''
        
        En bref : non, du moins pas ici.
        Bien sûr, cela embêterait Bob, mais garantir qu'une livraison arrive à destination n'est pas un problème qui se règle avec des cadenas.
        La sécurité soulève des centaines de questions, chacune s'étudiant avec des outils différents : et le problème étudié dans cette activité est de savoir si on peut, avec des cadenas, empêcher le livreur de \keyinfo{manger} le gâteau ou de l'\keyinfo{empoisonner}.
        Garantir que la livraison arrive à bon port se fait par d'autres moyens, qu'on va donc ignorer aujourd'hui.
        
        \item[\customtriangleright] \themaline{\emph{Détails et anecdotes}} :
        bloquer systématiquement une communication est appelé un \emph{déni de service} (ou \emph{DoS} pour \emph{denial of service}).
        Ce genre d'attaques (illégales) est fréquemment utilisé par des entreprises pour perturber les concurrents, majoritairement dans le milieu des Télécoms et de la finance~\cite{dos}.
        Le problème est également très présent dans... le jeu vidéo, où des équipes d'e-sport s'attaquent parfois pendant des compétitions.
      \end{enumerate}
    \end{commentbox}

  \subsection{Phase de compétition} \label{sec:compet}
    \timeinfo{15 min}

    Une fois la réflexion en équipe terminée, deux volontaires par groupe passent pour jouer leur proposition de protocole devant les autres.
    Ils jouent respectivement Isabelle et Bob, et l'animateur joue le livreur \keyinfo{de manière honnête}, c'est-à-dire, il n'essaie pas de voler le gâteau ou d'empoisonner Bob.
    % Cela exclut naturellement les ``\emph{et là vous pensez qu'il n'y a pas de gâteau dans le coffre et donc vous n'essayez pas de l'ouvrir}'' expliquant au livreur ce qu'il serait censé \keyinfo{croire ou ne pas faire}.
    Les autres groupes cherchent ensuite une attaque, c'est-à-dire, expliquent à l'animateur ce qu'il devrait faire pour empoisonner Bob.

    \begin{hardcommentbox}[Compétence technique\ ]
      Si le public ne trouve pas d'attaques sur la solution d'un groupe, l'animateur doit, dans l'idéal, déterminer \keyinfo{s'il n'en existe effectivement pas} (en omettant les attaques du même type que sur \versionlab{3}, où le livreur doit lui-même être client de la boulangerie).
      Cela demande un peu d'entraînement : de la méthodologie et des exercices corrigés sont compilés en \keyinfo{Annexe~\ref{sec:exo}} si besoin.

      \textbf{\itshape Autre possibilité :} faire chercher les attaques \keyinfo{seulement par les élèves} sans nécessairement intervenir, et présenter cette recherche d'attaque comme une phase d'interaction plus que comme une phase de correction d'exercice.
      Après tout, dans la vraie vie, il n'y a pas d'oracle magique pour rassurer un industriel sur la sécurité du système qu'il a mis en place.
      Si ce dernier a une faille cachée, elle sera peut-être relevée (ou pas) par un pair, mais \keyinfo{l'incertitude fait partie de la discipline} (aussi désagréable soit-elle).
      Le travail des chercheurs dans le domaine est typiquement de développer des méthodes et des outils rigoureux pour limiter cette incertitude.
    \end{hardcommentbox}

    \begin{commentbox}[Retours d'expérience] 
      À la fin du temps imparti, les groupes doivent présenter leur protocole \emph{en l'état}, même s'ils n'en sont pas satisfaits.
      Dans le pire des cas, cela fera toujours un entraînement d'attaque pour les autres groupes !
      D'où l'importance de circuler pendant la conception pour inciter à partir d'un protocole même naïf et de l'améliorer pas à pas --- cela évite de se retrouver sans rien à la fin.
      \begin{itemize}
        \item[\customtriangleright] \emph{\themaline{Anecdote}}: 
        La technique utilisée dans la solution, le protocole \versionlab{3}, est connue sous le nom de \emph{challenge-response} et est largement utilisée, par exemple dans les protocoles de paiement~\cite{EMV} et les passeports biométriques~\cite{passport}.
        Ici par exemple, Bob émet le ``challenge'' de lui renvoyer un gâteau accompagné de son long mot de passe temporaire :
        Isabelle étant la seule à pouvant ouvrir cette boîte, elle est ainsi censée être la seule à pouvoir répondre au challenge.
        Bien sûr, cela peut parfois être contourné en cas de faille dans structure du protocole,comme le montre l'attaque sur \versionlab{3} détaillée en Section~\ref{sec:v3-atk}.
      \end{itemize}
    \end{commentbox}

  \subsection{Dernière attaque et conclusion} \label{sec:complice}

    \timeinfo{10 min}

    Le public est maintenant assez fatigué et a compris que le problème était complexe.
    En fonction du temps et de la réactivité, soit :
    \begin{enumerate*}
      \item présenter la dernière attaque sur \versionlab{3} (voir Section~\ref{sec:v3-atk}), et la correction pour obtenir \versionlab{4} ; ou alors,
      \item directement conclure (en reprenant des éléments de la Section~\ref{sec:ccl}), éventuellement en mentionnant qu'il existe une attaque, que le public pourra chercher chez lui si intéressé, quand le livreur est client de la pâtisserie.
    \end{enumerate*}

    \begin{commentbox}[Retours d'expérience]
      Il peut arriver que certaines équipes aient trouvé un protocole résistant à cette attaque (en bref, un équivalent \versionlab{4} où Bob écrit son nom à l'intérieur de sa commande).
      Dans ce cas, il est possible de présenter \versionlab{3} comme le protocole d'une équipe fictive, puis de demander au public ce qu'il pense du protocole.
      \begin{enumerate}
        % \item Que l'attaque sur \versionlab{3} ait été présentée ou pas, le public est en général un peu abattu à la fin de l'activité, découragé par cette succession de faux espoirs de sécurité.
        % D'où l'importance que la conclusion soit à la fois rassurante, et tire une morale claire facile à retenir (ce que nous avons tenté de fournir en Section~\ref{sec:ccl}).
        % La partie sur l'importance des démonstrations vérifiée par ordinateur peut être un peu hermétique :
        % à réserver aux publics les plus matures et réceptifs.

        \item[\customtriangleright] \emph{\themaline{Anecdote}}: 
        Ce protocole de livraison de gâteaux (\versionlab{3}) est inspiré d'un protocole réel, dit de \emph{Needham-Schroeder} du nom de leurs inventeurs~\cite{NS78}.
        Comme ici, il souffre d'une attaque similaire à celle de \versionlab{3}.
        Cette attaque a seulement été découverte une vingtaine d'années après la publication du protocole, par Lowe~\cite{L96}, montrant que la recherche d'attaques n'est pas simple, même pour les experts y consacrant tout leur temps.
        Le problème étant que les analyses de l'époque avaient tendance à omettre inconsciemment la possibilité que l'\emph{attaquant} (le livreur dans l'énigme) ait la possibilité d'interagir avec les participants en tant que simple personne (ici, acheter un gâteau).
      \end{enumerate}
    \end{commentbox}
    