Formation pour animer une initiation à la cryptographie
===

Ce document décrit la mise en place d'une activité d'initiation aux thématiques de la cryptographie, sous la forme d'une énigme interactive modérée par un animateur, afin de former ce dernier aux quelques compétences de cryptographie élémentaire nécessaires.

  - **Animateur** : typiquement un.e \emph{professeur.e de mathématiques}, ou du moins une personne de formation scientifique attirée par la résolution de casse-têtes.
  
  - **Public** : un groupe de *jusqu'à 20 personnes* environ, *à partir de 13 ans*.
  D'expérience, fonctionne très bien avec un public d'adultes curieux.
  
  - **Durée** : environ 1h

  - **Objectifs** : faire prendre du recul au public sur la *notion de sécurité* en informatique, et lui faire prendre la mesure de la complexité de la conception de systèmes résistants aux interférences malveillantes.
  En particulier, cela peut donner une idée des métiers existant dans le domaine de la sécurité, ou du moins expliquer leur rôle et impact dans la société.
