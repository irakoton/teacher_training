COMPILER = pdflatex
BIB = bibtex

# temporary files
TEMP = *.aux *.log *.out *.nav *.snm *.toc *.blg *.bbl *.lof *.synctex.gz *.fdb_latexmk *.fls

# name of the folder
NAME = teacher_training

# one compilation round + open
all:
	make compil
	open main.pdf

# one compilation round
compil:
	$(COMPILER) main.tex

# biblio
bib:
	$(BIB) main

# removes temporary files
clean:
	rm -rf $(TEMP)

# zips the archive and sends the zip to the desktop
zip:
	cd .. && zip -r ~/Desktop/$(NAME).zip $(NAME)

# two rounds of compilation
pdf:
	make compil
	make compil
	open main.pdf