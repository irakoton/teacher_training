\section{L'histoire : l'énigme du livreur glouton} \label{sec:story}

  Tout d'abord, voici le \keyinfo{fil narratif} de l'activité, sans mention des interactions avec le public dont la logistique et les détails seront discutés plus tard (Sections~\ref{sec:material} et \ref{sec:outline}).
  Une version alternative de l'histoire, mettant l'accent sur d'autres thématiques et remplaçant la livraison de gâteaux par une variante de \keyinfo{Roméo et Juliette}, est détaillée en Section~\ref{sec:variant}.

  \subsection{Le décor} \label{sec:v1}
    Bob voudrait acheter un gâteau à la pâtisserie d'Isabelle.
    La boutique étant trop éloignée pour y aller en personne, il demande une livraison à domicile.
    Malheureusement, le livreur est connu pour sa gourmandise :
    il a tendance à manger compulsivement les gâteaux qu'il est censé livrer.
    De plus, pour des raisons de conjecture économique trop complexes pour de simples mortels, toutes les autres entreprises de livraison ont fait faillite.
    C'est une situation délicate :
    Isabelle et Bob \keyinfo{ont besoin du livreur}, mais savent qu'ils \keyinfo{ne peuvent pas lui faire confiance}.

    \begin{wrapfigure}{l}{0.44\textwidth}
      \centering
      \includegraphics[width=0.43\textwidth,page=1]{files/02-protocol.pdf}
    \end{wrapfigure}

    \noindent
    En raison de ce problème, ils se procurent chacun un \keyinfo{cadenas à clé} dans leurs villes respectives ;
    ainsi, quand ils utiliseront ce service postal peu fiable, ils pourraient mettre les livraisons dans des coffrets cadenassés pour éviter les vols.

    \begin{question} \label{q:v1}
      En se servant de ces cadenas, comment transféreriez-vous le gâteau depuis la pâtisserie d'Isabelle à la maison de Bob sans risque ?
    \end{question}

    La difficulté de l'énigme est que, bien qu'Isabelle et Bob aient tous deux un cadenas, ils ne peuvent pas ouvrir celui de l'autre.
    Isabelle pourrait mettre le gâteau dans un coffret, le verrouiller, et envoyer la clé à Bob plus tard;
    cependant le livreur pourrait alors obtenir à la fois le coffre verrouillé et sa clé, ce qui lui permettrait de l'ouvrir.
    Il faut donc trouver une solution où Isabelle et Bob gardent toujours leur clé sur eux.
    Voici une première idée.
    % 
    Dans un premier temps, Isabelle met le gâteau dans un coffre, le verrouille, et le fait livrer à Bob :
    \begin{center}
      \includegraphics[width=0.5\textwidth,page=2]{files/02-protocol.pdf}
    \end{center}

    Pour l'instant le livreur ne peut pas manger le gâteau, mais Bob ne peut pas non plus ouvrir le coffre.
    Ce dernier ajoute alors son propre cadenas au coffre de manière à ce qu'il soit verrouillé par les deux, puis retourne le colis à Isabelle :
    \begin{center}
      \includegraphics[width=0.5\textwidth,page=3]{files/02-protocol.pdf}
    \end{center}

    À la réception du coffret, Isabelle y retire son propre cadenas et le renvoie :
    \begin{center}
      \includegraphics[width=0.5\textwidth,page=4]{files/02-protocol.pdf}
    \end{center}

    Bob reçoit le gâteau dans un coffre scellé uniquement par son propre cadenas et peut donc le récupérer.
    Aucune clé n'a été envoyée durant le procédé et il y avait toujours au moins un cadenas sur chaque colis, empêchant le livreur de manger le gâteau.
    On appelle cela un : 
    
    \begin{defbox}[Protocole de sécurité :]
      suite d'instructions permettant d'envoyer un colis sensible à travers un réseau postal corrompu.
    \end{defbox}
    
  \subsection{Là où tout bascule} \label{sec:v1-atk}

    Appelons notre protocole de sécurité par double cadenas \versionlab{1}.
    Règle-t-il réellement le problème ?
    Il y a en fait de nombreuses façons pour le livreur de récupérer le gâteau, la plus directe étant de forcer les cadenas avec les outils appropriés.
    Notre protocole repose donc sur des hypothèses implicites, la première étant que les \keyinfo{cadenas sont incassables}.
    Une autre est que le livreur n'agit \keyinfo{que sur les livraisons} (il ne cambriole pas Isabelle et Bob pour voler leur clé).
    Bien que raisonnables, cela fait beaucoup d'hypothèses cachées...
    Y aurait-il encore autre chose sous le tapis ?
    % 
    Considérons le scénario de livraison suivant.
    Tout d'abord, Isabelle encoffre le gâteau avec son cadenas comme d'habitude.
    Mais là, le livreur \keyinfo{dévie de ses instructions} :
    au lieu de livrer Bob, il ajoute son propre cadenas --- acheté par ailleurs --- et retourne le colis.

    \begin{figure}[ht]
      \centering
      \includegraphics[width=0.5\textwidth,page=5]{files/02-protocol.pdf}
    \end{figure}

    N'ayant pas conscience qu'il ne s'agit pas du cadenas de Bob, Isabelle pense que le protocole suit son cours sans problème particulier.
    Elle retire donc son cadenas et demande au livreur de l'apporter à Bob :

    \begin{figure}[ht]
      \centering
      \includegraphics[width=0.5\textwidth,page=6]{files/02-protocol.pdf}
    \end{figure}

    La situation finale est alors la suivante :
    Isabelle n'a rien remarqué de particulier et le livreur peut déverrouiller le coffre avec sa propre clé.
    Cette \emph{attaque} n'a nécessité de forcer aucun cadenas, elle exploite simplement une brèche dans la structure du protocole.
    Mais surtout, elle révèle la dernière hypothèse cachée sur laquelle reposait notre solution :

    \begin{defbox}[Passivité :]
      malgré sa gourmandise, le livreur ne ment jamais sur le contenu d'un colis, c'est-à-dire, il suit le flot du protocole sans essayer activement de le perturber.
    \end{defbox}

  \subsection{Contre un livreur actif} \label{sec:v2}
    
    Effectuer cette attaque sur \versionlab{1} ne demande pas un effort énorme au livreur :
    il n'est donc pas raisonnable de supposer que ce dernier est passif lors de la conception de notre protocole.
    
    \begin{question} \label{q:v2}
      Comment envoyer un gâteau depuis la pâtisserie d'Isabelle à Bob sans risque qu'il se fasse manger par un livreur actif ?
    \end{question}

    Malheureusement, ce problème n'a pas de solution avec notre système de cadenas.
    Isabelle et ses clients décident donc de souscrire à une organisation à grande échelle de \emph{cadenas publics} :
    \begin{enumerate}
      \item tout membre de l'organisation se voit remettre une \keyinfo{clé personnelle};
      \item toute personne peut obtenir auprès de l'organisation un cadenas ne pouvant être ouvert que par la clé d'\keyinfo{un membre spécifique}.
    \end{enumerate}
    
    On supposera qu'on peut faire entièrement confiance à cette organisation et qu'elle a son propre réseau (non-corrompu) de distribution de cadenas.
    Un nouveau protocole --- appelons-le \versionlab{2} --- garantit alors la résistance au vol contre un livreur actif :
    Isabelle se procure le cadenas de Bob via l'organisation, l'utilise pour verrouiller le colis du gâteau, et le fait livrer.

  \subsection{Les livraisons dangereuses} \label{sec:v3}
    \begin{wrapfigure}{r}{0.30\textwidth}
      \centering
      \includegraphics[width=0.28\textwidth,page=3]{files/03-letter.pdf}
    \end{wrapfigure}

    Mais l'histoire ne s'arrête pas là.
    Vexé de ne pas pouvoir voler le gâteau, le livreur, mauvais perdant, se procure le cadenas de Bob chez l'organisation, et l'utilise pour lui envoyer un gâteau empoisonné avec le message ``\emph{Nous vous offrons ce gâteau en remerciement de votre fidélité. La pâtisserie d'Isabelle}''.
    Bob le mange, ce qui révèle une faiblesse du protocole :
    nous ne pouvons jamais être sûr que l'expéditeur d'une livraison est celui qu'il prétend être.
    Au retour de Bob de l'hôpital, Isabelle et lui décident de mettre à jour le protocole pour qu'il garantisse une forme d'\keyinfo{authentification}.

    \begin{question} \label{q:v3}
      Quel protocole \versionlab{3} Isabelle et ses clients pourraient suivre sans que 
      \begin{enumerate*}
        \item le gâteau puisse se faire voler, ni que
        \item le client puisse se faire empoisonner ?
      \end{enumerate*}
    \end{question}

    La question est complexe :
    tout ce qu'Isabelle fait, un livreur actif peut le faire, ce qui rend difficile pour elle de produire un colis différenciable à coup sûr d'un colis piégé.
    Voici une solution possible.
    % Isabelle et son client (ici Bob) se sont procuré chacun de leur côté, auprès de l'organisation, le cadenas de l'autre.
    Cette fois, le client est celui qui initie le protocole.
    Pour ce faire il écrit sur un bout de papier un (long) \keyinfo{mot de passe} de son choix qui servira à identifier sa commande.
    Il l'envoie ensuite à Isabelle dans un coffre scellé avec le cadenas de cette dernière (obtenu auprès de l'organisation), et joint une note au colis indiquant son nom à Isabelle :
    \begin{center}
      \includegraphics[width=0.5\textwidth,page=7]{files/02-protocol.pdf}
    \end{center}

    À la reception du coffre, Isabelle lit le nom du client sur la note, ouvre le coffre avec sa clé, met le gâteau à l'intérieur à côté du papier où est écrit le mot de passe, puis scelle le tout avec le cadenas du client (obtenu via l'organisation).
    Elle lui fait ensuite livrer le colis :
    \begin{center}
      \includegraphics[width=0.5\textwidth,page=8]{files/02-protocol.pdf}
    \end{center}

    Quand le client reçoit la commande, il vérifie que le gâteau est bien accompagné du même mot de passe qu'il a choisi initialement.
    Si ce n'est pas le cas il reste prudent, et détruit le gâteau au cas où il serait empoisonné.
    % 
    Intuitivement, la sécurité de ce protocole \versionlab{3} repose sur le fait que, pourvu que le mot de passe choisi par Bob soit assez long et imprévisible, si quelqu'un tentait d'envoyer à Bob un gâteau empoisonné en prétendant être Isabelle, il n'aurait qu'une probabilité négligeable de deviner le mot de passe par chance (et donc de faire manger le gâteau piégé à Bob).

  \subsection{Le comique de répétition} \label{sec:v3-atk}

    Avec ce protocole, nous avons enfin mis Bob en sécurité, le vol et l'empoisonnement étant à présent impossibles.
    Vraiment ?
    Si vous y avez cru, malheureusement, vous vous êtes encore faits avoir :
    comme pour \versionlab{1}, le raisonnement justifiant la sécurité de \versionlab{3} était trop informel pour être rigoureux.
    % C'est un problème que les mathématiciens connaissent bien :
    % tant qu'on ne s'est pas posé pour écrire proprement les hypothèses de son théorème, tout peut arriver d'ici arriver à en rédiger une démonstration...
    Ici, \versionlab{3} ne résout le Problème~\ref{q:v3} que sous une hypothèse implicite supplémentaire, qui cachait une autre attaque dans son ombre.
    Voyez-vous laquelle ?
    Considérez sinon le scénario de livraison suivant.
    Le protocole commence comme d'habitude :
    Bob met un mot de passe dans un coffre verrouillé, et écrit son nom sur une note jointe.
    Mais en parallèle, le livreur \keyinfo{achète aussi un gâteau} qui sera à récupérer plus tard à la pâtisserie.
    Il transmet alors la commande de Bob à Isabelle \emph{mais} remplace le nom de Bob par le sien :

    \begin{figure}[ht]
      \centering
      \includegraphics[width=0.6\textwidth,page=3]{files/04-protocol.pdf}
    \end{figure}

    Isabelle suit les instructions du protocole et met le gâteau dans le coffre (pensant que le mot de passe qui s'y trouve est celui du livreur), puis le referme \emph{avec le cadenas du livreur}.
    % À noter : cela ne compte \emph{pas} un vol de gâteau, puisque le livreur reçoit sa propre commande et non celle de Bob.
    Ce dernier ouvre alors le coffre, empoisonne le gâteau, et le referme avec le cadenas de Bob.
    \begin{center}
      \includegraphics[width=0.75\textwidth,page=2]{files/05-transform.pdf}
    \end{center}

    Le livreur donne ensuite la livraison piégée à Bob, que ce dernier croit sûre à cause de la présence de son mot de passe et mange le gâteau empoisonné.
    Notre erreur en concevant \versionlab{3} a été de ne pas protéger l'identité du client, en laissant la note indiquant son nom à l'air libre --- ce que peut exploiter le livreur comme dans l'attaque s'il a des \keyinfo{complices parmi les clients} de la pâtisserie ou en est lui-même un.
    % 
    La sécurité de \versionlab{3} repose donc sur le fait que le livreur n'achètera jamais de gâteau --- une hypothèse une fois de plus trop forte pour être raisonnable.
    Une contre-mesure naturelle est que le client mette la note avec son nom \keyinfo{à l'intérieur} du coffre, à côté du mot de passe, pour empêcher le livreur d'y toucher.
    Si on appelle \versionlab{4} cette version corrigée du protocole, elle permet donc d'éviter ce genre d'attaques.

  \subsection{Jusqu'à quand y croirez-vous ?} \label{sec:ccl}

    % ``Le protocole \versionlab{4} est sûr'' : y croirez-vous cette fois si on vous disait cela ?
    Après \versionlab{1} et \versionlab{3}, cela fait quand même deux fois que vous vous faites servir un protocole qui semblait offrir les garanties de sécurité attendues, avant de vous rendre qu'il y avait en fait une attaque en embuscade.
    Alors comment y croire cette troisième fois ?
    % 
    La réponse est simple : \emph{rien} ne permet raisonnablement d'y croire, du moins pas seulement sur la base d'arguments informels comme ceux qui vous ont été fournis.
    C'est la morale de toute cette histoire :
    la sécurité est une affaire extrêmement subtile et contre-intuitive, jamais aussi simple et binaire qu'elle peut en avoir l'air au premier abord.
    Vous l'avez vu tout du long : 
    la plupart des attaques étaient dues à des argumentations qui, bien que convaincantes, étaient trop peu rigoureuses et reposaient sur des hypothèses implicites déraisonnables.
    % 
    % \paragraph{Pourquoi tout ça ?}
      Tout cela est en fait une métaphore de la \keyinfo{cybersécurité} :
      \begin{enumerate}
        \item le gâteau représente une \emph{donnée sensible}, i.e., dont il est crucial qu'elle reste secrète (confidentialité) et ne se fasse pas altérer (intégrité). Exemple classique : les données bancaires ;
        \item les cadenas représentent la \emph{cryptographie}, plus précisément différents types de chiffrement, rendant un message inintelligible pour qui ne possède pas une clé de déchiffrement ;
        \item le livreur représente un \emph{réseau de communication sensible aux interceptions} (comme Internet, où il est possible d'interférer avec un signal en prenant le contrôle d'un relais).
      \end{enumerate}
      Vous comprendrez donc pourquoi, quand il est question de cybersécurité, il est apprécié que les garanties prétendues d'un système soient soutenues par une \keyinfo{démonstration} rigoureuse.
      En bref, une définition mathématique du protocole étudié et de la garantie de sécurité attendue, ainsi qu'une démonstration qu'elle est bien effective.

      % Cependant, même si les démonstrations en question prennent la même forme que celles de n'importe quel théorème de mathématiques, elles sont souvent \keyinfo{beaucoup plus fastidieuses}.
      % Il faut en effet considérer tous les scénarios possibles en présence d'un livreur actif (qu'on appelle un \emph{attaquant actif} en cybersécurité), ce qui représente facilement des milliers de cas à envisager pour des protocoles industriels.
      % Écrire une démonstration aussi longue a de forte chances de mener à \keyinfo{des erreurs} :
      % un humain, même le plus rigoureux et attentif d'entre tous, est sujet à la fatigue, et à terme à l'étourderie.
      % Pensez à un analogue :
      % vous êtes probablement confiants en votre capacité à poser une multiplication sans erreur d'inattention, mais le seriez-vous autant pour une multiplication de deux nombres de mille chiffres chacun ?
      
      % C'est pourquoi la communauté scientifique apprécie quand une démonstration de sécurité a pu être \keyinfo{générée ou vérifiée par ordinateur}.
      % En effet, une machine, elle, a rarement des problèmes de fatigue et d'étourderie !

    \paragraph{À emporter chez vous}
      Mais tous ces messages sont bien techniques, et il est peu probable que vous en ailliez la moindre utilité dans votre vie quotidienne.
      Retenez simplement que :
      \begin{enumerate}%[itemsep=1mm]
        \item \emph{La sécurité est une affaire complexe, mais surtout subtile}.
        Rien n'est ``100\% sécurisé'', ``inviolable'', toute sécurité n'est valable que sous certaines hypothèses, souvent techniques à comprendre pour le profane.
        % 
        {\em \sffamily Au quotidien, gardez donc du recul vis-à-vis des publicités ou gros titres de journaux avec des formules un peu trop optimistes !}
        
        \item \emph{La sécurité est un problème étudié par les scientifiques}.
        Des générations chercheurs se sont succédé pendant des décennies pour étudier beaucoup des systèmes déployés autour de nous, et ont conscience depuis bien longtemps des problèmes auxquels vous avez été confrontés dans ce document.
        % 
        {\em \sffamily Gardez donc du recul vis-à-vis des formules un peu trop pessimistes !}
      \end{enumerate}

      