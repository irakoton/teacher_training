\section{Gestion des contraintes horaires} \label{sec:time}
    
  De bout en bout, l'activité dure environ \keyinfo{1h15}, ce qui est souvent adapté aux cadres dédiés (ateliers lors de visites de laboratoire, fêtes de la science ou analogues...).
  Cela peut être plus compliqué en milieu scolaire où les cours sont souvent par créneaux de 55min.
  Possibilités :

  \begin{enumerate}[itemsep=1mm]
    \item \keyinfo{Trouver un créneau de 2x55min consécutives de cours}.
    Le cadre le plus adapté pour les classes de collège, une classe entière pouvant faire office de groupe (plus complexe pour le lycée où il faudrait plutôt une demi classe).
    Dans ce cas, une pause d'une dizaine de minutes peut être laissée aux élèves entre les deux créneaux, ce qui tombe souvent vers la fin de la phase de conception.
    En prenant son temps, l'activité s'étend ainsi facilement sur 1h40 environ.
    On peut alors compléter avec une séance de questions ou d'anecdotes sur le sujet ; les classes de collège apprécient par exemple qu'on parle d'exemples concrets comme le \emph{code de César}... et surtout sur comment le casser par \emph{analyse fréquentielle}, toutes les notions de mathématiques nécessaires étant justement au programme de collège !
    \item \keyinfo{Faire la séance sur deux crénaux non consécutifs de 55min}.
    L'analogue de la solution précédente pour les classes de lycée, ayant rarement plusieurs créneaux consécutifs en demi groupe.
    On fait la césure après la phase de conception, laissant la fin de l'activité à la séance suivante (en demi groupe ou classe entière).
    Bien que cela casse un peu le rythme de l'activité, la seconde séance garde un côté dynamique avec la compétition de la phase de restitution.
    Une fois la conclusion passée, il peut rester du temps pour poursuivre sur un cours classique, idéalement un sujet connexe pour les cours de sciences et technologie.
    \item \keyinfo{Faire la séance sur 55min}.
    Cela est possible, mais nécessite de faire quelques coupes.
    En comptant 5min d'installation et de 5min rangement, il reste environ 45min d'activité pure.
    Un bon découpage est alors de 5min de d'introduction, 10min pour l'énigme initiale, 15min pour la phase de conception, 10min pour la phase de restitution, et 5min de conclusion.
    On se limite alors à mentionner qu'une attaque sur \versionlab{3} existe si le livreur est client de la pâtisserie, sans la présenter, laissant les intéresser la chercher chez eux.
    Il ne faut pas traîner et éviter les détails et anecdotes, mais cela reste faisable en petit comité. 
  \end{enumerate}


\section{Entraînement technique} \label{sec:exo}

  Dans cette section finale sont compilés quelques conseils et exercices pour entraîner un non-expert en cryptographie à encadrer la phase de compétition (voir Section~\ref{sec:compet}).
  Aucune familiarité n'est pré-requise avec le domaine, mais la nature mathématique du problème pourrait rebuter les éventuels allergiques à la matière.

  % Pour rappel, le public est alors partagé en équipes tentant de concevoir le protocole \versionlab{3}, ce dernier étant censé offrir une solution au Problème~\ref{q:v3}, à savoir, le problème de livrer un gâteau tout en évitant les vols et les empoisonnements (voir Section~\ref{sec:v3}).
  % La difficulté est que l'animateur doit pouvoir déterminer si les solutions proposées par les différentes équipes sont attaquables ou non, et trouver les attaques correspondantes le cas échéant.

  \subsection{Principe fondamental des attaques étudiées}

    % \paragraph{La phrase magique}
      La plupart des attaques de l'activité reposent sur une observation simple :

      \begin{defbox}[Principe fondamental\ ]
        \itshape En dehors de l'utilisation de leur clé, toute action executable par Isabelle ou Bob peut également être effectuée par un livreur actif.
      \end{defbox}

      Par exemple, l'attaque sur \versionlab{1} (Section~\ref{sec:v1-atk}) utilise le fait que le livreur ``imite'' Bob :
      Bob doit ajouter son cadenas sur le colis qu'Isabelle lui envoie et le renvoyer, et c'est exactement ce que le livreur fait.
      % Cela résulte en une attaque puisque le protocole ne met aucun moyen en \oe uvre pour authentifier les cadenas.
      L'attaque sur \versionlab{2} (Section~\ref{sec:v3}) suit la même logique :
      Isabelle envoie ses gâteaux simplement sans aucune preuve de provenance, et le livreur peut donc faire de même avec un gâteau empoisonné.
      Le protocole \versionlab{3} est le premier de la série avec une forme d'authentification, via le mot de passe temporaire de Bob, d'où le fait que son attaque (Section~\ref{sec:v3-atk}) soit un peu plus subtile.
      Mais le but de la phase de conception étant précisément d'arriver à une sécurité du niveau de \versionlab{3}, vous ne serez confrontés qu'à des propositions soit ``aussi sûres que \versionlab{3}'', soit attaquables en utilisant principalement le principe fondamental.

      \medskip 
      \begin{itemize}[label = \customtriangleright]
        \item \themaline{\emph{Remarque}} : Le nom de ``\emph{principe fondamental}'' n'est pas classique --- ce n'est d'ailleurs pas du tout un principe central de la théorie de la sécurité.
        Il fallait cependant bien lui donner un nom dans ce document, au vu du nombre de fois dont nous allons en avoir besoin.
      \end{itemize}

    % \paragraph{En pratique}
      Voici un exemple simple, pour illustrer ce fameux principe en plus des attaques sur \versionlab{1} et \versionlab{2}.
      % Ce protocole pourrait très bien être la proposition d'une équipe lors de la phase de conception.
      Puisque le problème de \versionlab{2} est que Bob ne peut pas savoir si un colis d'Isabelle ou pas, ils mettent à jour le protocole simplement en demandant à Isabelle d'ajouter son nom à l'intérieur du colis en plus du gâteau :
      \begin{center}
        \includegraphics[width=0.6\textwidth]{files/06-example.pdf}
      \end{center}

      % La motivation est donc que Bob verra le nom d'Isabelle écrit à côté des gâteaux non empoisonnés.
      Naturellement, cette solution est très naïve, et il suffit d'appliquer le principe fondamental à Isabelle pour le voir :
      si Isabelle peut écrire son nom à l'intérieur d'un coffre, le livreur peut tout aussi bien écrire ``\emph{Isabelle}'' à côté de son gâteau empoisonné, résultant en une attaque.

  \subsection{Exercices corrigés}
    Pour conclure ce document, voici une série d'exercices compilant des situations concrètes (la plupart ayant été rencontrées sur le terrain).
    Chaque exercice décrit les étapes d'un protocole, supposément proposé par une équipe lors de la phase de conception, sa description étant ponctuée d'arguments (valides ou pas) comme vous pourriez typiquement en entendre en pratique.
    L'objectif à chaque fois est de :
    \begin{enumerate}
      \item \emph{Déterminer si le protocole est valide ou pas}, i.e., si le protocole utilise des mécanismes interdits (le cas échéant, il faut le signaler à l'équipe en question lors d'une activité).
      \item \emph{Déterminer si le protocole est attaquable}, en supposant que le livreur n'est pas client de la boulangerie.
      Pour rappel, cela est à garder pour vous lors de la phase de conception.
    \end{enumerate}

    \setlist{itemsep=1mm,topsep=0pt,parsep=0pt,leftmargin=*}

    \begin{exercise} \label{ex:challenge-and-drop}
      \begin{enumerate}
        \item[]
        \item Isabelle initie le protocole en choisissant un long mot de passe, qu'elle écrit sur un papier, qu'elle met dans un coffre fermé avec le cadenas de Bob et fait livrer le tout.
        \item \label{it:challenge-and-drop-read} Bob retire le cadenas, lit le mot de passe et renvoie le colis à Isabelle après l'avoir fermé avec le cadenas de cette dernière.
        \item \label{it:challenge-and-drop-last} À la réception, Isabelle vérifie que le coffre contient bien le même mot de passe que celui qu'elle a mis initialement.
        Si c'est bien le cas, elle a l'assurance qu'elle communique bien avec Bob, et peut donc lui envoyer le gâteau seul dans un coffre fermé avec le cadenas de Bob.
      \end{enumerate}
    \end{exercise}

    \begin{solution}
      Le protocole est valide (toutes les opérations qu'il réalise sont autorisées).
      En ce qui concerne la sécurité, bien que le début du protocole puisse paraître intéressant avec une utilisation de mot de passe, tout s'écroule à l'étape~\ref{it:challenge-and-drop-last} où Isabelle envoie finalement le gâteau sans aucun mot de passe, ce qui rend le colis final complètement décorrélé des étapes précédentes.
      Une attaque simple est par exemple pour le livreur de laisser les deux premières étapes se passer normalement, puis de mettre un gâteau empoisonné dans un colis fermé avec le cadenas de Bob, et de livrer le tout à ce dernier en prétendant qu'il s'agit du colis final d'Isabelle.
      % 
      % Les équipes proposant ce protocole comprennent souvent bien qu'il y a une forme d'authentification à effectuer pour résoudre le problème, mais considèrent étrangement que, une fois cette authentification faite après les deux premières étapes, le livreur ne peut plus usurper l'identité des personnages quoi que ces derniers fassent.
    \end{solution}


    \begin{exercise} \label{exo:isa-mdp}
      Même protocole qu'à l'Exercice~\ref{ex:challenge-and-drop}, sauf qu'à l'étape~\ref{it:challenge-and-drop-last}, Isabelle met le mot de passe \emph{avec} le gâteau.
      À la réception de ce colis, Bob ne mange le gâteau que s'il est accompagné du mot de passe qu'il a lu à l'étape~\ref{it:challenge-and-drop-read} du protocole.
    \end{exercise}

    \begin{solution}
      Le protocole est tout aussi valide qu'à l'Exercice~\ref{ex:challenge-and-drop}, et a cette fois une dernière étape utilisant l'échange de mot de passe effectué préalablement.
      La solution ressemble d'ailleurs à \versionlab{3} --- qui est pour rappel considérée comme sûr pendant l'activité puisque le livreur ne peut pas l'attaquer sans acheter lui-même de gâteau.
      La seule différence est qu'ici, \emph{Isabelle} choisit le mot de passe ; et c'est ce qui rend le protocole attaquable, en utilisant le principe fondamental.
      En effet, le livreur peut simplement imiter Isabelle, c'est-à-dire, initier des échanges avec Bob en lui envoyant tout ce que Isabelle lui aurait envoyé :
      \begin{enumerate}
        \item Le livreur choisit un mot de passe et l'envoie à Bob dans un coffre fermé.
        \item Ce dernier lui renvoie le colis fermé avec le cadenas d'Isabelle. 
        Le livreur ne peut pas l'ouvrir mais peu importe car il connaît son contenu (le mot de passe qu'il a lui-même choisi).
        Il jette donc le colis à la poubelle, et en fait un autre contenant le mot de passe écrit sur un nouveau papier et un gâteau empoisonné.
        \item Bob trouve bien le mot de passe qu'il a lu précédemment, et se fait donc empoisonner.
      \end{enumerate}

      Morale : le but étant de protéger \emph{Bob}, puisque le livreur peut toujours imiter Isabelle, Bob doit participer activement au processus d'authentification (comme dans \versionlab{3} où il est celui qui choisit le mot de passe), sous peine d'être sujet à une attaque du même type que ci-dessus.
      Cette attaque n'est cependant pas simple, et peut passer inaperçue en cas d'inattention, même pour des experts.
    \end{solution}


    \begin{exercise}
      \begin{enumerate}
        \item[]  
        \item Isabelle fait un double de sa clé et l'envoie à Bob dans un coffre fermé avec le cadenas de ce dernier.
        \item Une fois la clé reçue, on se retrouve dans la situation du tout début de l'activité, sauf que Isabelle et Bob partagent la clé d'un même cadenas.
        Isabelle peut donc envoyer le gâteau dans un coffre fermé par son propre cadenas sans risque d'interception.
      \end{enumerate}
    \end{exercise}

    \begin{solution}
      Le protocole est valide, mais l'argumentation comparant le protocole avec le début de l'activité est un argument fallacieux :
      non, on ne se retrouve pas du tout dans la même situation qu'au début de l'énigme, puisque n'importe qui peut contacter l'organisation pour obtenir les cadenas de n'importe qui.
      L'attaque consiste donc simplement à laisser Isabelle donner sa clé à Bob, puis à envoyer à Bob un gâteau empoisonné dans un coffre fermé avec le cadenas d'Isabelle.
    \end{solution}
    
    
    \begin{exercise}
      \begin{enumerate}
        \item[]
        \item Isabelle envoie tout d'abord un colis vide à un client tiers (appelons-le Charlie).
        \item Pendant que le livreur est occupé à faire cette livraison, Isabelle et Bob se rencontrent pour faire l'échange de gâteaux en main propre sans risque d'interférence.
      \end{enumerate}
    \end{exercise}
    
    \begin{solution}
      Cette solution est \emph{invalide}, et de manière générale, envoyer des boîtes vides est inutile (tous les colis doivent de toute façon passer par le livreur :
      l'occuper ailleurs ne fait que retarder la prochaine étape du protocole, sans avantage en contrepartie).
      Si Isabelle et Bob pouvaient se rencontrer en personne pour s'échanger le gâteau, ils auraient pas besoin du livreur en premier lieu ; 
      et surtout, il n'y aurait aucune nécessité de l'occuper ailleurs non plus.
      % Ce genre de solution a été proposé plusieurs fois par des publics jeunes de l'activité, qui ont parfois du mal avec le fait que le livreur soit plus une entité abstraite qu'un réel humain avec des limitations physique comme ne pas pouvoir être à plusieurs endroits à la fois.
    \end{solution}
    
    
    \begin{exercise}
      \begin{enumerate}
        \item[]
        \item Isabelle envoie un gâteau \emph{empoisonné} à Bob, dans un coffre sans cadenas.
        \item Si ce dernier ne le reçoit pas, c'est que le livreur l'a mangé et est donc mort, et Isabelle et Bob peuvent alors se retrouver sans risque d'interférence.
        \item Si Bob reçoit le gâteau, c'est que le livreur n'a pas l'intention d'interférer avec le protocole, et Isabelle peut envoyer un gâteau (non empoisonné) sans danger, dans un coffre fermé avec le cadenas de Bob.
      \end{enumerate}
    \end{exercise}
    
    \begin{solution}
      Cette solution est \emph{invalide}.
      Tout d'abord, il est absurde de ``tuer'' le livreur, puisque toutes les communications doivent passer par lui.
      Par ailleurs, si Isabelle et Bob pouvaient se retrouver en personne pour s'échanger le gâteau, le protocole serait inutile de toute façon.
      Enfin, le dernier point fait des hypothèses fortes sur le raisonnement du livreur, ce qui est déraisonnable puisqu'il sait ce qu'Isabelle et Bob prévoient de faire (voir protocoles par obscurité, point~\ref{it:obscurity} p.\pageref{it:obscurity}).
    \end{solution}

    
    \begin{exercise}
      \begin{enumerate}
        \item[]
        \item Isabelle choisit tout d'abord un long de passe \(m\) et met le gâteau et \(m\) (écrit sur un papier) dans un coffre fermé avec son \emph{propre} cadenas. Appelons le colis verrouillé qui en résulte \(c_1\).
        \item Isabelle met ensuite le colis \(c_1\) dans une boîte plus grande, y ajoute un autre papier avec \(m\) écrit dessus, puis ferme le tout avec le cadenas de Bob.
        Appelons ce nouveau colis \(c_2\), qui est envoyé à Bob.
        \item \label{it:read-mdp} À la réception de \(c_2\), Bob retire son cadenas, lit le mot de passe \(m\) à l'intérieur, puis referme la boîte avec le cadenas d'Isabelle.
        Appelons ce nouveau colis \(c_2'\), qui est envoyé à Isabelle.
        \item À la réception de \(c_2'\), Isabelle retire son cadenas pour obtenir \(c_1\) qui se trouve à l'intérieur (et qui, pour rappel, contient le gâteau et un papier avec \(m\)).
        Elle enlève le cadenas de \(c_1\) et le remplace par le cadenas de Bob, et lui envoie le colis \(c_1'\) résultant.
        \item À la réception de \(c_1'\), Bob retire son cadenas, et vérifie qu'il contient un gâteau et le mot de passe \(m\) qu'il a lu à l'étape~\ref{it:read-mdp}. Si c'est le cas, il peut manger le gâteau.
      \end{enumerate}
    \end{exercise}

    \begin{solution}
      Ce protocole est assez long à décrire, mais offre ainsi un bon entraînement à gérer les propositions denses (ce qui peut arriver en pratique, bien que rare), et avec des boîtes en poupées russes.
      Ce protocole est valide mais, malgré sa complexité, peut se casser très simplement en utilisant encore le principe fondamental.
      Le problème est similaire à l'Exercice~\ref{exo:isa-mdp} :
      rien n'empêche le livreur de simplement imiter Isabelle auprès de Bob car ce dernier n'est pas activement engagé dans le choix des mots de passe.
      L'attaque est la suivante :
      \begin{enumerate}
        \item Le livreur choisit un mot de passe \(m\) quelconque et construit le colis \(c_2\) (avec un gâteau empoisonné, mais en utilisant aussi les mêmes cadenas qu'Isabelle utiliserait, en particulier, il utilise le cadenas d'Isabelle pour construire \(c_1\) puisque c'est ce que Bob s'attend à voir).
        Le colis est ensuite livré à Bob.
        \item Bob reçoit un colis qu'Isabelle aurait très bien pu faire elle-même, et suit donc le protocole et construit \(c_2'\) en remplaçant le cadenas externe par celui d'Isabelle.
        \item Le livreur ne peut certes pas ouvrir \(c_2'\), mais peu importe, il le jette simplement et fabrique \(c_1'\) de zéro, avec une autre boîte, ce qui est possible puisqu'il a choisi lui-même \(m\).
        \item À la réception de \(c_1'\), Bob mange le gâteau empoisonné. \qedhere
      \end{enumerate}
    \end{solution}


    \begin{exercise}
      \begin{enumerate}
        \item[]
        \item Bob ouvre un nouveau compte à l'organisation, à un nouveau nom jamais utilisé jusqu'à aujourd'hui --- disons \emph{Bobo}. 
        Il reçoit donc une nouvelle clé, et l'organisation est prête à fournir le cadenas de Bobo à qui le demandera.
        \item Bob envoie ensuite à Isabelle, dans un coffre fermé avec le cadenas de cette dernière, une note indiquant le nom de Bobo.
        \item Isabelle peut alors obtenir le cadenas de Bobo auprès de l'organisation, et envoie le gâteau à Bob dans un coffre fermé avec ce cadenas (dont Bob a la clé, et dont l'attaquant ne connaît pas le nom du propriétaire).
      \end{enumerate}
    \end{exercise}

    \begin{solution}
      % Il est difficile de statuer sur le cas de ce protocole, car il joue avec des problèmes qui n'ont pas été mentionnés dans les retours d'expérience de la Section~\ref{sec:conception}.
      % Cependant aucune équipe n'a jamais proposé de protocole de ce type lors du déploiement de l'activité, d'où le fait que l'exercice est laissé en annexe de ce document, comme simple précaution.

      % Le problème est que le fonctionnement de l'organisation de cadenas publics est assez peu détaillée dans l'énigme, puisque \versionlab{3} et \versionlab{4} peuvent être conçues avec des interactions minimales avec elle.
      % Ce n'est pas le cas de ce protocole, ce qui pose un certain nombre de questions :
      % peut-on enregistrer des comptes dans cette organisation \emph{publique} sans que le livreur soit au courant ?
      % Est-ce que le livreur peut retrouver le propriétaire d'un cadenas public à sa vue ?
      % Ces questions font qu'il n'est pas clair si le protocole est réellement valide ou pas.
      % Notre conseil serait de le considérer comme \emph{invalide}, en demandant à l'équipe d'éviter les interactions avec l'organisation autre que la demande de cadenas.
      Ce genre de protocole est facilement attaquable en utilisant le principe fondamental.
      En effet, si le livreur imite \emph{Bob}, cela lui permet de voler le gâteau avec l'attaque suivante :
      \begin{enumerate}
        \item Le livreur crée un nouveau compte à l'organisation, appelons-le par exemple \emph{Bobi} ;
        \item Il donne à Isabelle un coffre fermé avec le cadenas de cette dernière, contenant une note avec le nom ``Bobi'' à l'intérieur ;
        \item Isabelle pense que ce nouveau compte est celui de Bob, et fait livrer le gâteau dans un coffre fermé avec le cadenas de Bobi, que le livreur peut donc ouvrir. \qedhere
      \end{enumerate}
    \end{solution}

    
    \begin{exercise}
      \begin{enumerate}
        \item[] 
        \item Isabelle choisit un long mot de passe et appelle Bob au téléphone pour lui communiquer.
        \item Isabelle envoie ensuite à Bob un coffre fermé avec le cadenas de ce dernier et contenant le gâteau et le mot de passe convenu écrit sur un papier.
        \item À la réception du colis, Bob vérifie qu'il contient le mot de passe convenu, et refuse de manger le gâteau sinon.
      \end{enumerate}
    \end{exercise}

    \begin{solution}
      Cette solution est sûre mais \emph{invalide} :
      comme expliqué au point~\ref{it:phone} p.\pageref{it:phone}, l'utilisation de canaux de communication en dehors du livreur sont interdits.
    \end{solution}

    
    \begin{exercise}
      \begin{enumerate}
        \item[]
        \item Isabelle choisit un long mot de passe et l'envoie à son client dans un coffre fermé avec le cadenas de ce dernier.
        \item \label{it:mdp} Le client (Bob ici) choisit un mot de passe également, l'ajoute dans le colis reçu, et renvoie le tout à Isabelle fermé avec le cadenas de cette dernière.
        \item Si Isabelle reçoit un colis avec deux mots de passe dont le sien, elle ajoute le gâteau dans le coffre, le ferme avec le cadenas de Bob, et fait livrer le tout.
        \item À la réception du colis, Bob ne mange le gâteau que s'il y trouve les deux mêmes mots de passe qu'à l'étape~\ref{it:mdp}.
      \end{enumerate}
    \end{exercise}

    \begin{solution}
      Ce protocole est valide.
      Intuitivement, il est identique à \versionlab{3}, si ce n'est qu'on ajoute au colis un mot de passe choisi par Isabelle, en plus de celui de Bob.
      Le protocole est donc aussi sûr que \versionlab{3} et donc accepté pour le phase de conception de l'activité.
      En revanche, l'absence d'indications d'identité à l'intérieur des boîtes comme dans \versionlab{4} le rend faible au même type d'attaque que \versionlab{3}, où le livreur commande un gâteau de son côté, empoisonne sa propre commande, et arrive à la faire accepter à Bob.
      En détails, l'attaque se déroule comme suit :
      \begin{enumerate}
        \item Le livreur commande un gâteau à Isabelle, qui choisit un mot de passe \(m_I\) et l'envoie au livreur dans un coffre fermé avec le cadenas de ce dernier.
        \item Quand Bob commande le gâteau à Isabelle, le livreur ne relaie pas sa demande et lui donne en retour un coffre contenant \(m_I\), fermé avec le cadenas de Bob.
        \item Bob prenant ce colis pour la réponse d'Isabelle, il l'ouvre, y ajoute un mot de passe de son choix \(m_B\), et referme le tout avec le cadenas d'Isabelle.
        \item Le livreur transmet ce colis à Isabelle, et prétend qu'il s'agit de sa réponse pour sa propre commande.
        Isabelle ouvre donc le coffre, y trouve \(m_I\) et \(m_B\) (et pense que \(m_B\) est le mot de passe du livreur comme dans l'attaque contre \versionlab{3}), y ajoute le gâteau, et ferme le colis avec le cadenas du livreur.
        \item On retrouve alors la fin de l'attaque sur \versionlab{3} :
        le livreur ouvre son colis, empoisonne le gâteau à l'intérieur, referme le tout avec le cadenas de Bob, et le donne à ce dernier qui y trouve \(m_I\) et \(m_B\) et mange donc le gâteau empoisonné.
      \end{enumerate}

      Cette attaque n'est pas fondamentalement plus difficile à comprendre que celle sur \versionlab{3}, mais son nombre substantiellement plus élevé d'étapes peut la rendre un peu (trop) ardue à trouver pour un animateur formé à la cryptographie seulement via ce document.
      Dans le cadre de cette activité, il est parfaitement acceptable de se satisfaire des propositions visiblement aussi sûres que \versionlab{3} comme ici, et de ne pas s'embêter outre mesure à chercher ce genre attaques complexes par usurpation d'identité.
      Présenter \versionlab{3} à la fin de la phase de conception comme la proposition d'une équipe fictive --- et dérouler l'attaque apprise par c\oe ur si le public ne la trouve pas --- est largement suffisant.
      Rappelez-vous simplement que, même si l'attaque en question est trop complexe à trouver pour vous, les protocoles ne contenant aucune information sur les identités à l'intérieur des colis sont a priori attaquables avec un scénario du style ``\emph{le livreur et Bob font une commande en parallèle, le livreur fait passer la commande de Bob pour la sienne, ce qui lui permet de l'empoisonner puis de la faire accepter à Bob malgré tout}''.
    \end{solution}

    
    % \begin{exercise}
    %   \begin{enumerate}
    %     \item[]
    %     \item Isabelle envoie à Bob un coffre fermé avec le cadenas de ce dernier et contenant le gâteau et un papier qu'Isabelle a signé de sa main.
    %     \item À la réception, Bob n'accepte de manger le gâteau que s'il trouve dans le colis la signature d'Isabelle.
    %   \end{enumerate}
    % \end{exercise}

    % \begin{solution}
    %   Ce protocole est considéré comme \emph{invalide} car il utilise des signatures (voir point~\ref{it:sign} p.\pageref{it:sign}).
    %   Cette solution fonctionnerait cependant avec une \emph{signature digitale}, un mécanisme cryptographique ayant entre autre les propriétés suivantes :
    %   \begin{enumerate}
    %     \item les signatures \emph{ne peuvent pas être imitées} (discutable pour des signatures papier) ;
    %     \item les signatures sont spécifiques au colis qu'elles accompagnent (alors que si le livreur obtient un jour la signature papier d'Isabelle, par exemple en lui faisant signer un accusé de réception, il pourrait l'utiliser pour tromper Bob dans le protocole ci-dessus). \qedhere
    %   \end{enumerate}
    % \end{solution}

    
    \begin{exercise}
      \begin{enumerate}
        \item[]
        \item Isabelle écrit sur un papier un mot de passe construit à partir des lettres de son prénom (par exemple, \emph{sleabIle}), le met dans un coffre, avec un gâteau, fermé avec le cadenas de Bob, et fait livrer le tout à ce dernier.
        \item Quand Bob reçoit le colis, il reconnaît les lettres du nom d'Isabelle et comprend que le colis a été envoyé par cette dernière, et peut donc manger le gâteau.
      \end{enumerate}
    \end{exercise}

    \begin{solution}
      Ce protocole peut être considéré comme valide, mais n'a aucune forme de sécurité.
      Il repose uniquement sur de la \emph{sécurité par obscurité} (voir point~\ref{it:obscurity} p.\pageref{it:obscurity}), c'est-à-dire, sur le fait que le livreur ne disposerait pas de la description du protocole --- ce qui est faux.
      Dans le cas présent, le livreur peut aisément empoisonner Bob en lui envoyant un colis fermé avec le cadenas de Bob et contenant un gâteau empoisonné et le message ``sleabIle''.
    \end{solution}

    